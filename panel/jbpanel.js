/**
 * Created by mircea.stanciu on 10/10/2014.
 */

function panel_delete(object, data, data2) {
    console.log('please overwrite');
}

function panel_drop(event, ui) {
    console.log('please overwrite');
}

function panel_click(object, data, data2) {
    console.log('please overwrite');
}

jQuery(document).ready(function () {

    jQuery('[data-action-drag]').draggable({
        containment: 'html',
        helper: 'clone'
    });

    jQuery('[data-action-drop]').droppable({
        drop: function (event, ui) {
            panel_drop(event, ui);
        }
    });

    jQuery(document).on('click', '[data-action-click]', function () {
        panel_click(jQuery(this), jQuery(this).data('action-click'), jQuery(this).data('element-type'));
    });


    jQuery(document).on('click', '[data-action-delete]', function () {
        panel_delete(jQuery(this), jQuery(this).data('action-delete'), jQuery(this).data('element-type'));
    });


});